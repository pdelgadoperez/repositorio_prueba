#include <stdio.h>
#include <string.h>

class A{
public:
	A(){a = 3;}
	int a;
	virtual void metodo(int a){printf("Estoy en A \n");}
};

class B: public A{
public:
	B(){a = 1;}
	int a;
	void metodo(float a){printf("Estoy en B float \n");}
};


int main(){
	A a, *pa=&a;
	B b;
	b.metodo(4.3);
	pa=&b;
	printf("%d\n", pa->a);
	printf("%d\n", (dynamic_cast<B*>(pa)->a));
}
