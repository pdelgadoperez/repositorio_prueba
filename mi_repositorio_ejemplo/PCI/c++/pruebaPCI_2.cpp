#include <stdio.h>
#include <string.h>

class A{
public:
	virtual void metodo(int a){printf("Estoy en A \n");}
};

class B: public A{
public:
	void metodo(float a){printf("Estoy en B float \n");}
};


int main(){
	A a, *pa=&a;
	B b;
	b.metodo(4.3);
	pa=&b;
	pa->metodo(4.3);
	(dynamic_cast<B*>(pa))->metodo(4.3);
}
