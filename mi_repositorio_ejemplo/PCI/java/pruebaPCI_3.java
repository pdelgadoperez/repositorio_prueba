class Parent {
	public String metodo(double a) {return "Parent";}
}
 
class Child extends Parent {
	public String metodo(int a) {return "Child int";}
}
 
public class pruebaPCI_3 {
	public static void main(String[] args) {
	Child c = new Child();
	Parent p = c;
	System.out.println(p.metodo(4));
	System.out.println(((Child)p).metodo(4));
	}
}
