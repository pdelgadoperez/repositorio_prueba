class Parent {
	public int v = 3;
	public String metodo(double a) {return "Parent";}
}
 
class Child extends Parent {
	public int v = 1;
	@Override
	public String metodo(double v) {return "Child double";}
}
 
public class pruebaPCI_2 {
	public static void main(String[] args) {
	Child c = new Child();
	Parent p = c;
	System.out.println(p.v);
	System.out.println(((Child)p).v);
	}
}
